This is a simple web project done by me for a college assestment. 

System deployment steps:

Client
	1/Install node js on the machine
	2/cd into the server folder, which contains the package.json file and open a terminal from there.
	3/Run "npm install" and you will see a new folder called node_modules is created.
	4/Run "npm start" and your Web server will up and run on PORT:8000
	
Server
	1/First run "npm i create-react-app"
	2/cd into the client folder,which contains the package.json file and open a terminal from there.
	3/Run "npm install" and you will see a new folder created called node_modules.
	4/Run "npm start" and your React application will start on PORT:3000

Read the final report to get an idea on how to work with the application.
Have fun!